package com.telecable.tdn.api.model

import java.io.Serializable
import com.google.gson.annotations.SerializedName as name

data class Radio(val uid: String = "", val name: String = "",
                 val image_url: String = "",
                 val description: String = "",
                 val date_description: String = ""
) : Serializable