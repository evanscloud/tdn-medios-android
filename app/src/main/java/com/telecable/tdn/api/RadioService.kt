package com.telecable.tdn.api

import com.telecable.tdn.api.model.Channel
import com.telecable.tdn.api.model.Radio
import com.telecable.tdn.api.model.RadioStat
import com.telecable.tdn.api.model.SongRequest
import com.telecable.tdn.util.PermissionDeniedException
import com.telecable.tdn.util.executor.ExecutorProvider
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestoreException
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposables
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RadioService @Inject constructor(
        private val api: ApiService,
        private val executors: ExecutorProvider
) {

    object Collections {
        const val RADIOS = "radio"
        const val CHANNELS = "channels"
        const val SONG_REQUEST = "songRequest"
    }

    private val firestore = Firestore.instance

    fun radioStats(): Observable<RadioStat> = api.radioStats()

    fun radios(): Observable<List<Radio>> {
        return Observable.create<List<Radio>> { observer ->
            firestore
                    .collection(Collections.RADIOS)
                    .addSnapshotListener(executors.io(), EventListener { snapshot, exception ->
                        if (null != exception) {
                            return@EventListener observer.onError(exception)
                        }
                        if (null != snapshot && !observer.isDisposed) {
                            snapshot.map { it.toObject(Radio::class.java) }.also(observer::onNext)
                        }
                    }).also { registration ->
                        observer.setDisposable(Disposables.fromAction {
                            registration.remove()
                        })
                    }
        }
    }

    fun channels(): Observable<List<Channel>> {
        return Observable.create<List<Channel>> { observer ->
            firestore
                    .collection(Collections.CHANNELS)
                    .addSnapshotListener(executors.io(), EventListener { snapshot, exception ->
                        if (null != exception) {
                            return@EventListener observer.onError(exception)
                        }
                        if (null != snapshot && !observer.isDisposed) {
                            snapshot.map { it.toObject(Channel::class.java) }.also(observer::onNext)
                        }
                    }).also { registration ->
                        observer.setDisposable(Disposables.fromAction {
                            registration.remove()
                        })
                    }
        }
    }

    fun requestSong(request: SongRequest): Single<Boolean> {
        return Single.create { emitter ->
            firestore.collection(Collections.SONG_REQUEST)
                    .add(request)
                    .addOnSuccessListener(executors.io(), OnSuccessListener {
                        emitter.onSuccess(true)
                    })
                    .addOnFailureListener(executors.io(), OnFailureListener {
                        // map any refuse by security-rule on firestore to a permission denied.
                        val denied = FirebaseFirestoreException.Code.PERMISSION_DENIED
                        val exc = if (it is FirebaseFirestoreException && it.code == denied) PermissionDeniedException(it) else it
                        emitter.onError(exc)
                    })
        }
    }
}