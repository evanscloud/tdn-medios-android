package com.telecable.tdn.util.executor

import java.util.concurrent.Executor

interface ExecutorProvider {
    fun io(): Executor
}