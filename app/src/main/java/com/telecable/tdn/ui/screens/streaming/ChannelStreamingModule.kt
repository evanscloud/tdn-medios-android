package com.telecable.tdn.ui.screens.streaming

import com.telecable.tdn.di.FragmentScoped
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class ChannelStreamingModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun channelStreamingFragment(): ChannelStreamingFragment

    @Module
    companion object {
        @JvmStatic
        @Provides
        fun provideStreamingURL(activity: ChannelStreamingActivity): String =
                activity.intent.extras.getString(ChannelStreamingActivity.STREAMING_URL_EXTRAS)
    }
}