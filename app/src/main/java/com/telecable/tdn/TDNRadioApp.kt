package com.telecable.tdn

import com.telecable.tdn.di.DaggerAppComponent
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class TDNRadioApp : DaggerApplication() {

    companion object {
        val REMOTE_CONFIG: FirebaseRemoteConfig by lazy {
            FirebaseRemoteConfig
                    .getInstance()
                    .apply { setDefaults(R.xml.remote_config) }
        }
    }
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
            DaggerAppComponent
                    .builder()
                    .application(this)
                    .build()
}