package com.telecable.tdn.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class TDNGlideModule : AppGlideModule()