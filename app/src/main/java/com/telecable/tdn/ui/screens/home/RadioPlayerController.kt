package com.telecable.tdn.ui.screens.home

interface RadioPlayerController {
    fun play()
    fun pause()
}