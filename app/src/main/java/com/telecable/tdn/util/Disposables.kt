package com.telecable.tdn.util

import io.reactivex.disposables.Disposable
import java.util.concurrent.atomic.AtomicBoolean

class Disposables {
    companion object {
        fun create(block: () -> Unit) : Disposable = BlockDisposable(block)
    }
}

class BlockDisposable(private val block: () -> Unit) : AtomicBoolean(), Disposable {

    override fun isDisposed(): Boolean = get()

    override fun dispose() = block().also { set(true) }
}