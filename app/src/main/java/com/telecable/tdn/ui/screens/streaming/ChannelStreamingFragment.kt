package com.telecable.tdn.ui.screens.streaming

import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.telecable.tdn.R
import com.telecable.tdn.di.ActivityScoped
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.channel_streaming_fragment.*
import javax.inject.Inject

@ActivityScoped
class ChannelStreamingFragment @Inject constructor(): DaggerFragment(), Player.EventListener {

    @field:[Inject JvmField]
    var streamingURL: String = ""

    private lateinit var player: SimpleExoPlayer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.channel_streaming_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        startChannelStreaming()
    }

    override fun onPause() {
        super.onPause()
        if (::player.isInitialized) {
            player.playWhenReady = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if(::player.isInitialized)
            player.release()
    }

    override fun onPlayerError(error: ExoPlaybackException?) {
        error?.printStackTrace()
        Log.e("***", "", error)

        val adb = AlertDialog.Builder(activity)
                .setTitle(R.string.video_no_reproduced_title)
                .setMessage(R.string.video_no_reproduced_message)
                .setPositiveButton(R.string.accept) { _, _ ->
                    activity?.finish()
                }
                .create()

        adb.show()
    }

    override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
    }

    override fun onSeekProcessed() {
    }

    override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
    }

    override fun onLoadingChanged(isLoading: Boolean) {
    }

    override fun onPositionDiscontinuity(reason: Int) {
    }

    override fun onRepeatModeChanged(repeatMode: Int) {
    }

    override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
    }

    override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        when (playbackState) {
            Player.STATE_BUFFERING -> {
                videoLoadingIndicator.visibility = View.VISIBLE
            }
            Player.STATE_READY -> {
                videoLoadingIndicator.visibility = View.GONE
            }
        }
    }

    private fun startChannelStreaming() {
        loadVideo(streamingURL)
    }

    private fun loadVideo(channelUrl: String) {
        val bandWithMeter = DefaultBandwidthMeter()
        val trackSelector = DefaultTrackSelector(bandWithMeter)

        player = ExoPlayerFactory.newSimpleInstance(activity, trackSelector)
        streamingPlayer.player = player

        val defaultBandwidthMeter = DefaultBandwidthMeter()
        val dataSourceFactory = DefaultDataSourceFactory(activity, "TDNApp", defaultBandwidthMeter)

        val hlsMediaSource = HlsMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(channelUrl))

        player.addListener(this)
        player.prepare(hlsMediaSource)
        streamingPlayer.requestFocus()
        player.playWhenReady = true
    }
}