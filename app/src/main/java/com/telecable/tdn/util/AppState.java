package com.telecable.tdn.util;

import java.util.concurrent.atomic.AtomicBoolean;

public final class AppState {
    public static AtomicBoolean isForeground = new AtomicBoolean(true);
}
