package com.telecable.tdn.util.ext

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

fun FragmentManager.bind(fragment: Fragment, @IdRes to: Int) {
    val transaction = this.beginTransaction()
    transaction.add(to, fragment)
    transaction.commit()
}