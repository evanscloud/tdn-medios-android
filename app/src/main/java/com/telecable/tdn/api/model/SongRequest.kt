package com.telecable.tdn.api.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp
import java.io.Serializable

data class SongRequest(val name: String, val song: String, val greeting: String = "", @ServerTimestamp val created:Timestamp? = null) : Serializable
