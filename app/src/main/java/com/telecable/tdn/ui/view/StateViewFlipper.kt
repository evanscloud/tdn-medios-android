package com.telecable.tdn.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.ViewFlipper

class StateViewFlipper constructor(context: Context?, attrs: AttributeSet? = null) : ViewFlipper(context, attrs) {

    constructor(context: Context?) : this(context, null)

    companion object {
        const val CONTENT = 0
        const val EMPTY = 1
        const val LOADING = 2
    }

    override fun onDetachedFromWindow() = try {
        super.onDetachedFromWindow()
    } catch (ex: Exception) {
        stopFlipping()
    }

    fun setLoading(){
        flipTo(LOADING)
    }

    fun setEmpty(){
        flipTo(EMPTY)
    }

    fun setContent(){
        flipTo(CONTENT)
    }

    private fun flipTo(position: Int){
        animateFirstView = true
        displayedChild = position
    }
}