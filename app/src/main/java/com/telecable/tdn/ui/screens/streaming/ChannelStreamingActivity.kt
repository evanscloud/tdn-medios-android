package com.telecable.tdn.ui.screens.streaming

import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.telecable.tdn.R
import com.telecable.tdn.util.ext.bind
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class ChannelStreamingActivity : DaggerAppCompatActivity() {

    companion object {
        const val STREAMING_URL_EXTRAS = "com.evanscloud.tdn.ui.screens.streaming.STREAMING_URL_EXTRAS"
    }

    @Inject
    lateinit var fragment: ChannelStreamingFragment

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideSystemUI()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.channel_streaming_activity)

        if (null == supportFragmentManager.findFragmentById(R.id.container)) {
            supportFragmentManager.bind(fragment, R.id.container)
        }
    }

    private fun hideSystemUI() {
        var flags: Int = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            flags = flags or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        }
        window.decorView.systemUiVisibility = flags
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }
}