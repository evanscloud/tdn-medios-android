package com.telecable.tdn.ui.screens.home.music

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.firestore.FieldValue
import dagger.android.support.DaggerFragment
import com.telecable.tdn.R
import com.telecable.tdn.api.RadioService
import com.telecable.tdn.api.model.SongRequest
import com.telecable.tdn.di.ActivityScoped
import com.telecable.tdn.ui.screens.home.HomeActivity
import com.telecable.tdn.ui.state.UiState
import com.telecable.tdn.util.ext.addTo
import com.telecable.tdn.util.scheduler.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.request_music_fragment.*
import javax.inject.Inject

@ActivityScoped
class RequestMusicFragment @Inject constructor() : DaggerFragment() {

    companion object {
        const val TAG = "**RequestMusicFragment**"
    }

    @Inject
    lateinit var service: RadioService

    @Inject
    lateinit var schedulers: SchedulerProvider

    private val subscriptions = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.request_music_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sendButton.setOnClickListener {
            requestSong()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.clear()
    }

    private fun requestSong() {
        subscriptions.clear()
        if (validFields()) {
            service.requestSong(getRequest())
                    .toObservable()
                    .map { UiState.success(it) }
                    .onErrorReturn { UiState.error(it) }
                    .startWith(UiState.loading())
                    .subscribeOn(schedulers.io())
                    .observeOn(schedulers.ui())
                    .subscribe {
                        when {
                            it.isLoading() -> showLoading()
                            it.isSuccess() -> {
                                hideLoading()
                                successMessage()
                            }
                            it.isError() -> {
                                hideLoading()
                                failedMessage()
                            }
                        }
                    }
                    .addTo(subscriptions)
        }
    }


    private fun showLoading() {
        (activity as? HomeActivity)?.setActivityIndicator(true)
    }

    private fun hideLoading() {
        (activity as? HomeActivity)?.setActivityIndicator(false)
    }

    private fun successMessage() {
        Toast.makeText(activity!!, R.string.song_request_success, Toast.LENGTH_LONG).show()

        val adb = AlertDialog.Builder(activity!!)
                .setTitle(R.string.song_request_successs_title)
                .setMessage(R.string.song_request_successs_body)
                .setPositiveButton(R.string.accept) { _, _ ->  }
                .create()

        adb.show()

        formFieldName.text.clear()
        formFieldSong.text.clear()
        formFieldGreeting.text.clear()
    }

    private fun failedMessage() {
        Toast.makeText(activity!!, R.string.song_request_failed, Toast.LENGTH_LONG).show()
    }

    private fun validFields(): Boolean {
        if (formFieldName.text.isEmpty()) {
            formFieldName.error = getString(R.string.field_name)
            return false
        }
        formFieldName.error = null
        if (formFieldSong.text.isEmpty()) {
            formFieldSong.error = getString(R.string.field_song)
            return false
        }
        formFieldSong.error = null
        return true
    }

    private fun getRequest() = SongRequest (
            name = formFieldName.text.toString(),
            song = formFieldSong.text.toString(),
            greeting = formFieldGreeting.text.toString()
    )
}