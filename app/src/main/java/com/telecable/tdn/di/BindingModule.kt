package com.telecable.tdn.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.telecable.tdn.ui.screens.home.HomeActivity
import com.telecable.tdn.ui.screens.home.HomeModule
import com.telecable.tdn.ui.screens.streaming.ChannelStreamingActivity
import com.telecable.tdn.ui.screens.streaming.ChannelStreamingModule

@Module
interface BindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [HomeModule::class])
    fun homeActivity(): HomeActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [ChannelStreamingModule::class])
    fun channeelStreamingActivity(): ChannelStreamingActivity
}