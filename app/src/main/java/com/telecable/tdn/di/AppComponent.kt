package com.telecable.tdn.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import com.telecable.tdn.TDNRadioApp
import com.telecable.tdn.api.ApiModule
import com.telecable.tdn.network.NetworkModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    BindingModule::class,
    AppModule::class,
    ApiModule::class,
    NetworkModule::class,
    AndroidInjectionModule::class
])
interface AppComponent : AndroidInjector<TDNRadioApp>{

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }
}