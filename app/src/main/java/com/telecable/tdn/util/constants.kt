package com.telecable.tdn.util

object Constants {

    // connection timeouts
    const val READ_TIMEOUT      = 30L // 30 seconds
    const val WRITE_TIMEOUT     = 30L
    const val CONNECT_TIMEOUT   = 30L
}