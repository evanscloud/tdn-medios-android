package com.telecable.tdn.network

import android.content.Context
import io.reactivex.Observable
import io.reactivex.Single

interface NetworkManager {
    fun isNetworkAvailableValue(): Boolean

    fun isNetworkAvailable(): Single<Boolean> = Single.just(isNetworkAvailableValue())

    fun connectivityOn(context: Context): Observable<Boolean>
}