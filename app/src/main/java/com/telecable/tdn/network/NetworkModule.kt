package com.telecable.tdn.network

import dagger.Module
import dagger.Provides
import com.telecable.tdn.BuildConfig
import com.telecable.tdn.util.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    fun networkManager(impl: ConnectivityNetworkManager): NetworkManager = impl


    @Provides
    @Singleton
    fun okHttpClient(
            @Named("is_debug") isDebug: Boolean,
            loggingInterceptor: HttpLoggingInterceptor,
            @Named("read_timeout") readTimeout: Long,
            @Named("write_timeout") writeTimeout: Long,
            @Named("connect_timeout") connectTimeout: Long
    ): OkHttpClient = OkHttpClient
            .Builder()
            .readTimeout(readTimeout, TimeUnit.SECONDS)
            .writeTimeout(writeTimeout, TimeUnit.SECONDS)
            .connectTimeout(connectTimeout, TimeUnit.SECONDS)
            .also {
                if (isDebug)
                    it.addInterceptor(loggingInterceptor)
            }
            .build()


    @Provides
    @Singleton
    fun loggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }


    @Provides
    @Named("api_url")
    fun apiURL(): String = BuildConfig.API_URL


    @Provides
    @Named("connect_timeout")
    fun connectTimeout(): Long = Constants.CONNECT_TIMEOUT

    @Provides
    @Named("read_timeout")
    fun readTimeout(): Long = Constants.READ_TIMEOUT

    @Provides
    @Named("write_timeout")
    fun writeTimeout(): Long = Constants.WRITE_TIMEOUT
}