package com.telecable.tdn.api

import io.reactivex.Observable
import com.telecable.tdn.api.model.RadioStat
import retrofit2.http.GET

interface ApiService {
    @GET("/stats?sid=1&json=1")
    fun radioStats() : Observable<RadioStat>
}