package com.telecable.tdn.util.executor

import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DefaultExecutorProvider @Inject constructor() : ExecutorProvider{

    companion object {
        private val IO: Executor by lazy {
            Executors.newCachedThreadPool()
        }
    }

    override fun io(): Executor = IO
}