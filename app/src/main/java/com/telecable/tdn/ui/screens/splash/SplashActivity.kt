package com.telecable.tdn.ui.screens.splash

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.telecable.tdn.ui.screens.home.HomeActivity

class SplashActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        val intent = Intent(this, HomeActivity::class.java)
        startActivity( intent)
        finish()
    }
}