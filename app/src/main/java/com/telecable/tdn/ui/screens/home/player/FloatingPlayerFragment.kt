package com.telecable.tdn.ui.screens.home.player

import android.app.Activity
import android.app.AlertDialog
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.gms.common.util.CrashUtils
import com.google.gson.Gson
import com.telecable.tdn.R
import com.telecable.tdn.TDNRadioApp
import com.telecable.tdn.api.model.RadioStat
import com.telecable.tdn.di.ActivityScoped
import com.telecable.tdn.network.NetworkManager
import com.telecable.tdn.ui.screens.home.HomeActivity
import com.telecable.tdn.ui.screens.home.RadioPlayerController
import com.telecable.tdn.ui.state.UiState
import com.telecable.tdn.ui.state.data
import com.telecable.tdn.util.AppState
import com.telecable.tdn.util.CustomLoadControl
import com.telecable.tdn.util.ext.addTo
import com.telecable.tdn.util.ifActive
import com.telecable.tdn.util.ifViewActive
import com.telecable.tdn.util.scheduler.SchedulerProvider
import dagger.android.support.DaggerFragment
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.floating_player_fragment.*
import okhttp3.*
import java.io.IOException
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

@ActivityScoped
class FloatingPlayerFragment @Inject constructor() : DaggerFragment(), RadioPlayerController {

    companion object {
        const val TAG = "**PlayerFragment**"
    }

    @Inject
    lateinit var httpClient: OkHttpClient

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var schedulers: SchedulerProvider

    @Inject
    lateinit var connectivity: NetworkManager

    private val subscriptions = CompositeDisposable()

    private var lastSongTitle = ""

    private var playerIsReady = AtomicBoolean(false)

    private var player: SimpleExoPlayer? = null

    private val rendersFactory: DefaultRenderersFactory by lazy {
        DefaultRenderersFactory(activity?.applicationContext)
    }
    private val trackSelectionFactory: AdaptiveTrackSelection.Factory by lazy {
        AdaptiveTrackSelection.Factory(DefaultBandwidthMeter())
    }
    private val trackSelector: DefaultTrackSelector by lazy {
        DefaultTrackSelector(trackSelectionFactory)
    }
    private val controls: LoadControl by lazy {
//        DefaultLoadControl()
        CustomLoadControl()
    }
    private val dataSourceFactory: DefaultDataSourceFactory by lazy {
        DefaultDataSourceFactory(activity?.applicationContext, "TDN")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.floating_player_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupComponents()
        bindComponents()
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.clear()
    }


    private fun setupComponents() {
        playerPausePlay.setOnClickListener {
            Log.e("**", "${player?.isPaused()} ${player?.isPlaying()} $lastSongTitle ${null == player} ${subscriptions.size()}")
            restorePausePlayButtonState()

            if (player?.isPlaying() == true) {
                pause()
            } else {
                play()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        restorePausePlayButtonState()
        AppState.isForeground.set(true)
    }

    override fun onPause() {
        super.onPause()
        AppState.isForeground.set(false)
    }

    private fun bindComponents() {
        val isConnected = connectivity
                .connectivityOn(activity!!)
                .subscribeOn(schedulers.io())
                .share()

        isConnected
                .take(1)
                .filter { connected -> !connected } // if disconnected show "disconnect" page
                .observeOn(schedulers.ui())
                .subscribe(
                        {
                            showNoConnectionMessage()
                        },
                        { Log.e(TAG, "error", it) }, { })
                .addTo(subscriptions)
    }

    private fun showSlowConnectionMessage() {
        activity.ifActive("showSlowConnectionMessage") {
            Toast.makeText(activity, R.string.slow_connection_message, Toast.LENGTH_LONG).show()
        }
    }

    private fun showNoConnectionMessage() {
        activity.ifActive("showNoConnectionMessage") {
            Toast.makeText(activity, R.string.no_network_connection, Toast.LENGTH_LONG).show()
        }
    }

    private fun showServerErrorMessage() {
        activity.ifActive("showServerErrorMessage") {
            Toast.makeText(activity, R.string.error_contacting_with_server, Toast.LENGTH_LONG).show()
        }
    }

    private fun setButtonToPlay() {
        playerPausePlay.ifViewActive("setButtonToPlay") {
            setImageResource(R.drawable.ic_play_circle_outline_white_36dp)
        }
    }

    private fun setButtonToPause() {
        playerPausePlay.ifViewActive("setButtonToPause") {
            setImageResource(R.drawable.ic_pause_circle_outline_white_36dp)
        }
    }

    private fun showLoading() {
        playerSongName.ifViewActive("showLoading") {
            text = getString(R.string.loading_text)
        }
        (activity as? HomeActivity)?.setActivityIndicator(true)
    }

    private fun hideLoading() {
        (activity as? HomeActivity)?.setActivityIndicator(false)
    }


    private fun restorePausePlayButtonState() {
        if (player?.isPlaying() == true && playerIsReady.get()) {
            setButtonToPause()
        } else {
            setButtonToPlay()
        }
    }

    private fun fetchConfigAndStartStreaming() {
        val config = TDNRadioApp.REMOTE_CONFIG

        config.fetch().addOnCompleteListener(activity!!) {
            if (it.isSuccessful) {
                config.activateFetched()

                updateTitles(config.getString(getString(R.string.radio_info_url_key)))
                startStreaming(config.getString(getString(R.string.radio_streaming_url_key)))
            } else {
                showServerErrorMessage()
            }
        }
    }

    private fun updateTitles(serverUrl: String) {
        scheduledStats(serverUrl)
                .map { UiState.success(it) }
                .onErrorReturn { UiState.error(it) }
                .startWith(UiState.loading())
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe { updateUI(it) }
                .addTo(subscriptions)
    }

    private fun scheduledStats(server: String): Observable<RadioStat> = Observable
            .timer(7, TimeUnit.SECONDS)
            .flatMap { getStats(server) }
            .flatMap {
                scheduledStats(server)
                        .take(1)
                        .startWith(it)
            }
            .subscribeOn(Schedulers.io())


    private fun updateUI(state: UiState<*>) {
        when {
            state.isLoading() -> {
                showLoading()
            }
            state.isError() -> {
                hideLoading()
                showServerErrorMessage()
            }
            state.isSuccess() -> {
                hideLoading()
                process(state.data<RadioStat>()!!)
            }
            else -> {
            }
        }
    }

    private fun getStats(serverUrl: String): Observable<RadioStat> {
        return Observable.create<RadioStat> { observer ->
            val request = Request.Builder()
                    .url(serverUrl)
                    .build()
            httpClient.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    if (!observer.isDisposed)
                        observer.onError(e)
                }

                override fun onResponse(call: Call, response: Response) {
                    val json = response.body()?.string()
                    json ?: return
                    if (!observer.isDisposed) {
                        observer.onNext(gson.fromJson(json, RadioStat::class.java))
                        observer.onComplete()
                    }
                }
            })
        }.subscribeOn(schedulers.io())
    }

    private fun process(stat: RadioStat) {
        val title = if(stat.songtitle.isEmpty()) stat.servertitle else stat.songtitle

        if (player?.isPlaying() == true && playerIsReady.get()) {
            playerSongName.ifViewActive("") {
                text = title
            }
        } else {
            lastSongTitle = title
        }
    }

    private fun startStreaming(streaming: String) {

        val source = ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(streaming))
        player?.prepare(source)
        player?.play()

        Handler(Looper.getMainLooper()).postDelayed({
            if (player?.isPaused() == true) {
                showSlowConnectionMessage()
            }
        }, TimeUnit.SECONDS.toMillis(5))
    }

    private fun onPlayerReady() {
        playerIsReady.set(true)
        setButtonToPause()
        if (lastSongTitle.isNotEmpty()) {
            playerSongName.ifViewActive {
                text = lastSongTitle
            }
        }
    }

    private fun onPlayerFailed(ex: Exception? = null) {
        Log.e(TAG, "player error", ex)
        playerIsReady.set(false)
        setButtonToPlay()
        showServerErrorMessage()
        playerSongName.ifViewActive {
            text = ""
        }
        player = null
    }

    override fun play() {
        if (null == player || !playerIsReady.get()) {
            if (null == player)
                player = createPlayer()
            fetchConfigAndStartStreaming()
        }
        else if (player?.isPaused() == true) {
            player?.play()
            setButtonToPause()
        }
    }

    override fun pause() {
        if (player?.isPlaying() == true) {
            player?.pause()
            setButtonToPlay()
        }
    }

    private fun createPlayer(): SimpleExoPlayer {
        return ExoPlayerFactory.newSimpleInstance(rendersFactory, trackSelector, controls).apply {
            addListener(PlayerEvents())
        }
    }

    private inner class PlayerEvents : Player.EventListener {
        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
        }

        override fun onSeekProcessed() {
        }

        override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
        }

        override fun onPlayerError(error: ExoPlaybackException?) {
            onPlayerFailed(error)
        }

        override fun onLoadingChanged(isLoading: Boolean) {
        }

        override fun onPositionDiscontinuity(reason: Int) {
        }

        override fun onRepeatModeChanged(repeatMode: Int) {
        }

        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
        }

        override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {
        }

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            when (playbackState) {
                Player.STATE_READY -> {
                    onPlayerReady()
                }
            }
        }
    }

    private fun SimpleExoPlayer.isPlaying(): Boolean = playWhenReady

    private fun SimpleExoPlayer.isPaused(): Boolean = !playWhenReady

    private fun SimpleExoPlayer.play() {
        playWhenReady = true
    }

    private fun SimpleExoPlayer.pause() {
        playWhenReady = false
    }
}