package com.telecable.tdn.util

class PermissionDeniedException(cause: Throwable) : RuntimeException(cause)