package com.telecable.tdn.ui.screens.home

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.View
import com.telecable.tdn.R
import com.telecable.tdn.ui.screens.home.channel.ChannelFragment
import com.telecable.tdn.ui.screens.home.music.RequestMusicFragment
import com.telecable.tdn.ui.screens.home.player.FloatingPlayerFragment
import com.telecable.tdn.ui.screens.home.radio.RadioFragment
import com.telecable.tdn.util.ext.bind
import dagger.android.support.DaggerAppCompatActivity
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.home_activity.*
import javax.inject.Inject

class HomeActivity : DaggerAppCompatActivity(), RadioPlayerController {

    @Inject
    lateinit var radioFragment: RadioFragment

    @Inject
    lateinit var channelFragment: ChannelFragment

    @Inject
    lateinit var requestMusicFragment: RequestMusicFragment

    @Inject
    lateinit var floatingPlayerFragment: FloatingPlayerFragment

    private val fragments: Array<DaggerFragment> by lazy {
        arrayOf(radioFragment, channelFragment, requestMusicFragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        setToolbar()
        setupTabs()
        attachFloatingPlayer()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
    }

    private fun setupTabs() {
        viewPager.adapter = TabAdapter(supportFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
            }
        })
    }

    private fun attachFloatingPlayer() {
        if (null == supportFragmentManager.findFragmentById(R.id.playerContainer)) {
            supportFragmentManager.bind(floatingPlayerFragment, R.id.playerContainer)
        }
    }

    fun setActivityIndicator(loading: Boolean = true) {
        activityIndicator.visibility = if (loading) View.VISIBLE else View.GONE
    }

    private inner class TabAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment = fragments[position]
        override fun getCount(): Int = 3
        override fun getPageTitle(position: Int): CharSequence? = when (position) {
            0 -> getString(R.string.radio_title)
            1 -> getString(R.string.tv_title)
            2 -> getString(R.string.music_title)
            else -> ""
        }
    }

    override fun play() = floatingPlayerFragment.play()

    override fun pause() = floatingPlayerFragment.pause()
}