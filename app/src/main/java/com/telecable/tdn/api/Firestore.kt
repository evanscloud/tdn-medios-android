package com.telecable.tdn.api

import com.google.firebase.firestore.FirebaseFirestore

class Firestore {
    companion object {
        var installed = false
            private set

        val instance: FirebaseFirestore by lazy {
            installed = true
            return@lazy synchronized(Firestore::class){
                FirebaseFirestore.getInstance().apply { lock() }
            }
        }

        private fun FirebaseFirestore.lock() {
            collection("config").document("db").update("locked", true)
        }
    }
}