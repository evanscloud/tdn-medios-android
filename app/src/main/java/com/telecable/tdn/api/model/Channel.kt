package com.telecable.tdn.api.model

import java.io.Serializable

data class Channel(val uid: String = "",
                   val name: String = "",
                   val url: String = "",
                   val description: String = "",
                   val image_url: String = ""
) : Serializable