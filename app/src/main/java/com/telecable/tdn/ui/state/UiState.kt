package com.telecable.tdn.ui.state

data class UiState<out Data>(val loading: Boolean = false, val data: Data? = null, val throwable: Throwable? = null){
    companion object {
        fun <Data> success(data: Data) = UiState(data = data)

        fun <Data> loading() = UiState<Data>(loading = true)

        fun <Data> empty() = UiState<Data>()

        fun <Data> error(throwable: Throwable) = UiState<Data>(throwable = throwable)
    }

    fun isSuccess() = null != data

    fun isLoading() = loading

    fun isError() = null != throwable

    fun isEmpty() = !isError() && !isLoading() && !isSuccess()
}

inline fun <reified T> UiState<*>.data(): T? = (data as? T)