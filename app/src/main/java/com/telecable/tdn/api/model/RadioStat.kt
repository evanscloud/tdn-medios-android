package com.telecable.tdn.api.model

import java.io.Serializable

data class RadioStat(
        val averagetime: Int,
        val backupstatus: Int,
        val bitrate: String,
        val content: String,
        val currentlisteners: Int,
        val maxlisteners: Int,
        val peaklisteners: Int,
        val samplerate: String,
        val servergenre: String,
        val servergenre2: String,
        val servergenre3: String,
        val servergenre4: String,
        val servergenre5: String,
        val servertitle: String,
        val serverurl: String,
        val songtitle: String,
        val streamhits: Int,
        val streamlisted: Int,
        val streamlistederror: Int,
        val streampath: String,
        val streamstatus: Int,
        val streamuptime: Int,
        val uniquelisteners: Int,
        val version: String
) : Serializable