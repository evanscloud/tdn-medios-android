package com.telecable.tdn.util

import android.app.Activity
import android.view.View
import com.crashlytics.android.Crashlytics

fun <T : View, R> T?.ifViewActive(caller: String = "", body: T.() -> R): R? {
    if (this != null && AppState.isForeground.get()) {
        return body()
    }
    Crashlytics.log(3, caller, "Got null view object (foreground = ${AppState.isForeground.get()})")
    return null
}

fun <T : Activity, R> T?.ifActive(caller: String = "", body: T.() -> R): R? {
    if (this != null && AppState.isForeground.get()) {
        return body()
    }
    Crashlytics.log(3, caller, "Got null activity object (foreground = ${AppState.isForeground.get()})")
    return null
}