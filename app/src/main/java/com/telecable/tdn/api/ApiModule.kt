package com.telecable.tdn.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun apiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun retrofit(@Named("api_url") url: String, converter: Converter.Factory, adapter: CallAdapter.Factory, client: OkHttpClient)
            : Retrofit = Retrofit
            .Builder()
            .baseUrl(url)
            .addCallAdapterFactory(adapter)
            .addConverterFactory(converter)
            .client(client)
            .build()


    @Provides
    @Singleton
    fun callAdapterFactory(): CallAdapter.Factory = RxJava2CallAdapterFactory.create()


    @Provides
    @Singleton
    fun converterFactory(gson: Gson): Converter.Factory = GsonConverterFactory.create(gson)


    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().setLenient().create()
}