package com.telecable.tdn.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import com.telecable.tdn.BuildConfig
import com.telecable.tdn.util.executor.DefaultExecutorProvider
import com.telecable.tdn.util.executor.ExecutorProvider
import com.telecable.tdn.util.scheduler.DefaultSchedulerProvider
import com.telecable.tdn.util.scheduler.SchedulerProvider
import javax.inject.Named
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Binds
    abstract fun bindApplication(app: Application): Context

    @Binds
    abstract fun bindSchedulers(impl: DefaultSchedulerProvider): SchedulerProvider

    @Singleton
    @Binds
    abstract fun executorProvider(provider: DefaultExecutorProvider): ExecutorProvider

    @Module
    companion object {
        @JvmStatic
        @Provides
        @Named("is_debug")
        fun provideIsDebug(): Boolean = BuildConfig.DEBUG
    }
}