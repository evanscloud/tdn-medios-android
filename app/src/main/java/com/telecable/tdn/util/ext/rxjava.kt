package com.telecable.tdn.util.ext

import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableContainer

fun <C : DisposableContainer> Disposable.addTo(container: C) = apply { container.add(this) }