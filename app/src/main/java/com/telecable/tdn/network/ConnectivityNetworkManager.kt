package com.telecable.tdn.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import io.reactivex.Observable
import io.reactivex.disposables.Disposables
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityNetworkManager @Inject constructor() : NetworkManager {

    @Inject
    lateinit var mContext: Context

    private val networkChanges: Subject<Boolean> by lazy { PublishSubject.create<Boolean>() }

    override fun isNetworkAvailableValue(): Boolean = hasNetworkConnection()

    override fun connectivityOn(context: Context): Observable<Boolean> {
        val filter = IntentFilter()
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        context.registerReceiver(networkChangesReceiver, filter)

        return Observable.create<Boolean> { observer ->
            val d = networkChanges.subscribe(observer::onNext, { }, { }) // /turn off error/complete propagation
            observer.setDisposable(Disposables.fromAction {
                stopConnectivityOn(context)
                d.dispose()
            })
        }
    }


    private fun stopConnectivityOn(context: Context) = try {
        context.unregisterReceiver(networkChangesReceiver)
    } catch (exc: Exception) {
        // ignore
    }


    private val networkChangesReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            networkChanges.onNext(isNetworkAvailableValue())
        }
    }


    private fun hasNetworkConnection(): Boolean =
            isConnectedAs(ConnectivityManager.TYPE_WIFI) || isConnectedAs(ConnectivityManager.TYPE_MOBILE)


    private fun isConnectedAs(type: Int): Boolean {
        val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        if (null != cm) {
            val info = cm.activeNetworkInfo
            return null != info && info.type == type && info.isAvailable && info.isConnected
        }
        return false
    }
}