package com.telecable.tdn.ui.screens.home

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.telecable.tdn.di.FragmentScoped
import com.telecable.tdn.ui.screens.home.music.RequestMusicFragment
import com.telecable.tdn.ui.screens.home.player.FloatingPlayerFragment
import com.telecable.tdn.ui.screens.home.radio.RadioFragment
import com.telecable.tdn.ui.screens.home.channel.ChannelFragment

@Module
abstract class HomeModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun radioFragment(): RadioFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun tvFragment(): ChannelFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun musicFragment(): RequestMusicFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun floatingPlayerFragment(): FloatingPlayerFragment
}