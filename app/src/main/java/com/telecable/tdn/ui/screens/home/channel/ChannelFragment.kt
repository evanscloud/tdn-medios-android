package com.telecable.tdn.ui.screens.home.channel

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import dagger.android.support.DaggerFragment
import com.telecable.tdn.R
import com.telecable.tdn.api.RadioService
import com.telecable.tdn.api.model.Channel
import com.telecable.tdn.di.ActivityScoped
import com.telecable.tdn.glide.GlideApp
import com.telecable.tdn.network.NetworkManager
import com.telecable.tdn.ui.screens.home.HomeActivity
import com.telecable.tdn.ui.screens.streaming.ChannelStreamingActivity
import com.telecable.tdn.ui.state.UiState
import com.telecable.tdn.ui.state.data
import com.telecable.tdn.util.AppState
import com.telecable.tdn.util.ext.addTo
import com.telecable.tdn.util.ifActive
import com.telecable.tdn.util.scheduler.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.channel_list_fragment.*
import kotlinx.android.synthetic.main.channel_list_item.view.*
import javax.inject.Inject

@ActivityScoped
class ChannelFragment @Inject constructor() : DaggerFragment() {
    companion object {
        const val TAG = "**ChannelFragment**"
    }

    @Inject
    lateinit var service: RadioService

    @Inject
    lateinit var schedulers: SchedulerProvider

    @Inject
    lateinit var connectivity: NetworkManager

    private val subscriptions = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.channel_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupComponents()
    }

    override fun onStop() {
        super.onStop()
        flipper.stopFlipping()
        subscriptions.clear()
    }

    override fun onPause() {
        super.onPause()
        AppState.isForeground.set(false)
    }

    override fun onResume() {
        super.onResume()
        if (connectivity.isNetworkAvailableValue()) {
            loadData()
        } else {
            bindComponents()
        }
        AppState.isForeground.set(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.clear()
    }

    private fun setupComponents() {
        channelList.adapter = ChannelsAdapter { openChannel(it) }
    }

    private fun openChannel(channel: Channel) {
        pauseRadio()

        val intent = Intent(activity, ChannelStreamingActivity::class.java)
        intent.putExtra(ChannelStreamingActivity.STREAMING_URL_EXTRAS, channel.url)
        startActivity(intent)
    }

    private fun pauseRadio() {
        (activity as? HomeActivity)?.pause()
    }

    private fun bindComponents() {
        val isConnected = connectivity
                .connectivityOn(activity!!)
                .subscribeOn(schedulers.io())
                .share()

        // handle initial disconnection
        isConnected
                .take(1)
                .filter { connected -> !connected } // if disconnected show "disconnect" page
                .observeOn(schedulers.ui())
                .subscribe(
                        {
                            flipper.setEmpty()
                        },
                        { Log.e(TAG, "error", it) }, { })
                .addTo(subscriptions)

        // handle initial connection
        isConnected
                .startWith(connectivity.isNetworkAvailableValue())
                .skipWhile { connected -> !connected }
                .take(1)
                .observeOn(schedulers.ui())
                .subscribe(
                        {
                            loadData()
                        },
                        { Log.e(TAG, "error", it) }, { })
                .addTo(subscriptions)
    }

    private fun loadData() {
        subscriptions.clear()

        service.channels()
                .map { UiState.success(it) }
                .onErrorReturn { UiState.error(it) }
                .startWith(UiState.loading())
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribe { updateUI(it) }
                .addTo(subscriptions)
    }

    private fun updateUI(state: UiState<*>) {
        when {
            state.isLoading() -> showLoading()
            state.isError() -> {
                hideLoading()
                flipper.setEmpty()
            }
            state.isEmpty() -> {
                hideLoading()
                flipper.setEmpty()
            }
            state.isSuccess() -> {
                hideLoading()
                val data = state.data<List<Channel>>() ?: emptyList()
                if (data.isEmpty()) {
                    flipper.setEmpty()
                } else {
                    setupList(data)
                    flipper.setContent()
                }
            }
        }
    }

    private fun showLoading() {
        activity.ifActive("ChannelFragment.showLoading") {
            (activity as? HomeActivity)?.setActivityIndicator(true)
        }
    }

    private fun hideLoading() {
        activity.ifActive("ChannelFragment.hideLoading") {
            (activity as? HomeActivity)?.setActivityIndicator(false)
        }
    }


    private fun setupList(channels: List<Channel>) {
        activity.ifActive("ChannelFragment.setupList") {
            (channelList.adapter as? ChannelsAdapter)?.refreshRadios(channels)
        }
    }


    private inner class ChannelsAdapter(
            private val channels: MutableList<Channel> = mutableListOf(),
            private val listener: (Channel) -> Unit
    ) : RecyclerView.Adapter<ChannelsAdapter.ItemHolder>() {

        fun refreshRadios(radios: List<Channel>) {
            this.channels.apply {
                clear()
                addAll(radios)
            }
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder =
                ItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.channel_list_item, parent, false))

        override fun getItemCount(): Int = channels.size

        override fun onBindViewHolder(holder: ItemHolder, position: Int) = holder.bind(channels[position])

        inner class ItemHolder(view: View) : RecyclerView.ViewHolder(view) {
            fun bind(channel: Channel) = with(itemView) {
                channelName.text = channel.name
                channelDescription.text = channel.description

                val transform = RequestOptions().apply {
                    transforms(CenterCrop(), RoundedCorners(16))
                }
                GlideApp.with(this@ChannelFragment)
                        .load(channel.image_url)
                        .placeholder(R.drawable.ic_live_tv_gray_24dp)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .apply(transform)
                        .into(channelImage)

                setOnClickListener { listener(channel) }
            }
        }
    }
}